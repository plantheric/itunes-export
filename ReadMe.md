#iTunes Export

Export and synchronise tracks from your iTunes library.

The use case for this application was to export the contents of my iTunes Library to a USB drive to be used in my car stereo.

iTunes Export is only able to export unprotected audio files.

iTunes Export requires the LAME mp3 encoder, this can be downloaded from the [LAME website](http://lame.sourceforge.net/index.php). You will need to build the macosx framework and place a copy in the folder with the other source files.
