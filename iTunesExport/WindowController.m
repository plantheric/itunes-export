//
//  WindowController.m
//  iTunesExport
//
//  Created by Mark Bartlett on 07/05/2013.
//  Copyright (c) 2013 plantheric. All rights reserved.
//

#import "WindowController.h"
#import "iTunesSource.h"
#import "NSArray+Extensions.h"
#import "NSURL+Extensions.h"
#import "Converter.h"
#import "Playlist.h"

@interface WindowController ()

@property NSURL *exportFolder;
@property NSArray *filesToExport;
@property iTunesSource *iTunesSource;
@property Converter *converter;
@property BOOL scanningFolder;

@end

@implementation WindowController

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
    }
    
    return self;
}

- (void)windowDidLoad {

    [super windowDidLoad];
}

- (void)awakeFromNib {

	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	
	BOOL isStale;
	NSURL* url = [NSURL URLByResolvingBookmarkData:[userDefaults dataForKey:@"ExportFolder"]
											options:NSURLBookmarkResolutionWithSecurityScope
									  relativeToURL:nil
								bookmarkDataIsStale:&isStale
											  error:nil];

	if (isStale == NO && [url startAccessingSecurityScopedResource] == YES) {
		self.exportFolder = url;
	}

	[self.bitrate selectItemWithTag:[userDefaults integerForKey:@"MP3Bitrate"]];
	[self.quality selectItemWithTag:[userDefaults integerForKey:@"MP3Quality"]];

	[self updateUI];
	
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
		
		iTunesSource *source = [[iTunesSource alloc] init];

		if ([source loadLibrary] == TRUE) {
			
			dispatch_async(dispatch_get_main_queue(), ^() {
				self.iTunesSource = source;
				[self generateExportList];
			});
		}
	});
}

- (void)windowWillClose:(NSNotification *)notification {

	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	
	NSData* bookmark = [self.exportFolder bookmarkDataWithOptions:NSURLBookmarkCreationWithSecurityScope
							includingResourceValuesForKeys:@[]
											 relativeToURL:nil
													 error:nil];
	
	[userDefaults setObject:bookmark forKey:@"ExportFolder"];
	[userDefaults setInteger:self.bitrate.selectedTag forKey:@"MP3Bitrate"];
	[userDefaults setInteger:self.quality.selectedTag forKey:@"MP3Quality"];
	[userDefaults synchronize];
}

- (IBAction)changeExportFolder:(id)sender {

	NSOpenPanel *panel = [NSOpenPanel openPanel];
	panel.canChooseFiles = NO;
	panel.canChooseDirectories = YES;
	
	if ([panel runModal] == NSFileHandlingPanelOKButton) {

		[self.exportFolder stopAccessingSecurityScopedResource];

		self.exportFolder = panel.URL;
		[self generateExportList];
	}
}

- (void)updateUI {

	if (self.exportFolder)
		self.exportLocation.stringValue = [self.exportFolder safePath];

	if (self.iTunesSource)
		self.libraryLocation.stringValue = [self.iTunesSource.libraryLocation safePath];
	
	if (self.scanningFolder == YES) {
		self.filesToExportCount.stringValue = @"Scanning…";
	} else {
		self.filesToExportCount.integerValue = [self.filesToExport count];
	}
	
	[self.exportButton setEnabled:self.exportFolder && self.iTunesSource && self.scanningFolder == NO];
	
	[self.exportButton setHidden: self.converter != nil];
	[self.cancelButton setHidden: self.converter == nil];
	[self.cancelButton setEnabled: self.converter != nil && self.converter.exportStopping == NO];
	[self.progress setHidden: self.converter == nil];
}

- (void)generateExportList {
	
	self.scanningFolder = YES;
	[self updateUI];
	
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
		
//		NSDate* start = [NSDate date];
		
		unsigned long iTunesLibraryBasePathLength = [[self.iTunesSource.libraryLocation path] length];
		unsigned long exportFolderBasePathLength = [[self.exportFolder path] length];
		
		NSArray *iTunesFiles = [[[self.iTunesSource tracksForMusicPlaylist] map:^(NSURL* url) {
			return [url path];
		}] sortedArrayUsingComparator:^(NSString* a, NSString* b) {
			return [a caseInsensitiveCompare:b];
		}];
		
		NSArray *existingFiles = [[[self scanExportFolder] map:^(NSURL *url) {
			return [[[url path] substringFromIndex:exportFolderBasePathLength] stringByDeletingPathExtension];
		}] sortedArrayUsingComparator:^(NSString* a, NSString* b) {
			return [a caseInsensitiveCompare:b];
		}];

		__block NSUInteger indexStart = 0;

		NSArray* missingFiles = [iTunesFiles filter:^(NSString *absolutePath){

			if ([absolutePath length] > iTunesLibraryBasePathLength) {
				NSString *relative = [[absolutePath substringFromIndex:iTunesLibraryBasePathLength] stringByDeletingPathExtension];
				
				NSUInteger match = [existingFiles indexOfObjectAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(indexStart, existingFiles.count - indexStart)]
																 options:NSEnumerationConcurrent
															 passingTest:^(NSString* str, NSUInteger idx, BOOL *stop) {
																return (BOOL)(([relative caseInsensitiveCompare:str] == NSOrderedSame) ? YES : NO);
															}];
				if (match != NSNotFound) {
					indexStart = match+1;
				}
				return (BOOL)(match == NSNotFound ? YES : NO);
			} else {
				NSLog(@"short path - %@", absolutePath);
				return NO;
			}
		}];
		
		self.filesToExport = [missingFiles map:^(NSString* path) {
			return [NSURL fileURLWithPath:path isDirectory:NO];
		}];

//		NSLog(@"Export list %@", self.filesToExport);
//		NSLog(@"Export list duration %f", [start timeIntervalSinceNow]);

		dispatch_async(dispatch_get_main_queue(), ^(void) {
			
			self.scanningFolder = NO;
			[self updateUI];
		});
	});
}

- (NSArray*)scanExportFolder {
	
	NSMutableArray *files = [NSMutableArray array];
	
	if (self.exportFolder) {
		NSDirectoryEnumerator *directoryEnum = [[NSFileManager defaultManager] enumeratorAtURL: self.exportFolder
																	includingPropertiesForKeys: @[NSURLIsDirectoryKey]
																					   options: NSDirectoryEnumerationSkipsHiddenFiles
																				  errorHandler: nil];
		for (NSURL *file in directoryEnum) {
			NSDictionary *attributes = [file resourceValuesForKeys:@[NSURLIsDirectoryKey] error:nil];
			
			// Add full path for non directories
			if ([attributes[NSURLIsDirectoryKey] boolValue] == NO)
				[files addObject:file];
		}
	}
	
	//NSLog(@"files - %@", files);
	return files;
}

- (IBAction)exportFiles:(id)sender {
	
	self.converter = [[Converter alloc] initWithSourceFolder:self.iTunesSource.libraryLocation exportFolder:self.exportFolder];

	self.converter.mp3Bitrate = (int)self.bitrate.selectedTag;
	self.converter.mp3Quality = (int)self.quality.selectedTag;

	[self updateUI];

	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
		
		[self.converter exportFiles:self.filesToExport withProgress:^(double progress, NSString *fileName) {

			[self.progress setDoubleValue:progress];
			self.exportStatus.stringValue = [NSString stringWithFormat:@"Exporting '%@\'…", fileName];
		}];
		
		self.exportStatus.stringValue = @"Exporting playlists…";

		NSArray *playlists = [self.iTunesSource userPlaylistNames];
		
		for (NSString *playlistName in playlists) {
			NSArray *tracks = [self.iTunesSource tracksForPlaylistNamed:playlistName];
			
			Playlist *playlist = [[Playlist alloc] initWithSourceFolder:self.iTunesSource.libraryLocation exportFolder:self.exportFolder];
			[playlist writePlaylistNamed:playlistName withTracks:tracks];
		}
		
		self.exportStatus.stringValue = @"";
		self.converter = nil;
		
		dispatch_async(dispatch_get_main_queue(), ^(void) {
			
			[self generateExportList];
		});
	});
}

- (IBAction)cancelExport:(id)sender {
	[self.converter stopExport];
	[self updateUI];
}


@end
