//
//  AppDelegate.m
//  iTunesExport
//
//  Created by Mark Bartlett on 05/05/2013.
//  Copyright (c) 2013 plantheric. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {

}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender {
	return YES;
}

@end
