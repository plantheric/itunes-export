//
//  iTunesSource.m
//  iTunesExport
//
//  Created by Mark Bartlett on 07/05/2013.
//  Copyright (c) 2013 plantheric. All rights reserved.
//

#import <iTunesLibrary/ITLibrary.h>
#import <iTunesLibrary/ITLibMediaItem.h>
#import <iTunesLibrary/ITLibPlaylist.h>

#import "iTunesSource.h"

#import "NSArray+Extensions.h"

@interface iTunesSource()

@property ITLibrary* iTunesLibrary;

@end

@implementation iTunesSource

- (id)init
{
	self = [super init];
	if (self) {

		self->_libraryLocation = [NSURL URLWithString:@""];
	}
	return self;
}

- (BOOL)loadLibrary {
	
	NSError* error;
	self.iTunesLibrary = [ITLibrary libraryWithAPIVersion:@"1.0" error:&error];

	NSURL *musicFolder = self.iTunesLibrary.musicFolderLocation;
	self->_libraryLocation = [musicFolder URLByAppendingPathComponent:@"Music"];
	
	return self.iTunesLibrary != nil;
}

- (NSArray*)tracksForMusicPlaylist {
	
	NSArray* musicTracks = [self.iTunesLibrary.allMediaItems filter:^(ITLibMediaItem* item) {
		
		BOOL include = item.mediaKind == ITLibMediaItemMediaKindSong &&
						item.drmProtected == NO &&
						item.location.isFileURL;
		return include;
	}];

	NSArray* trackLocations = [musicTracks map:^(ITLibMediaItem* item) {
		return item.location;
	}];

	return trackLocations;
}

- (NSArray*)userPlaylistNames {
	
	NSArray* playlists = [self.iTunesLibrary.allPlaylists filter:^(ITLibPlaylist* playlist) {
		return (BOOL)(playlist.distinguishedKind == ITLibDistinguishedPlaylistKindNone && playlist.master == NO && playlist.visible == YES);
	}];
	
	NSArray* playlistNames = [playlists map:^(ITLibPlaylist* playlist) {
		return playlist.name;
	}];
	
	
//	NSLog(@"userPlaylistNames %@", playlistNames);
	return playlistNames;
}

- (NSArray*)tracksForPlaylistNamed:(NSString*)playlistName {
	
	ITLibPlaylist* playlist = [self.iTunesLibrary.allPlaylists first:^(ITLibPlaylist* playlist) {
		return [playlist.name isEqualToString:playlistName];
	}];
	
	NSArray* tracks = [playlist.items filter:^(ITLibMediaItem* item) {
		return item.location.isFileURL;
	}];

	NSArray* trackLocations = [tracks map:^(ITLibMediaItem* item) {
		return item.location;
	}];
	
//	NSLog(@"tracksForPlaylistNamed %@, %@", playlistName, trackLocations);
	return trackLocations;
}

@end
