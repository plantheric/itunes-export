//
//  WriteFile.h
//  iTunesExport
//
//  Created by Mark Bartlett on 11/05/2013.
//  Copyright (c) 2013 plantheric. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Track;

@interface WriteFile : NSObject

- (id)initWithURL:(NSURL*)url track:(Track*)track;
- (BOOL)create;
- (BOOL)write:(NSData*)sampleData last:(BOOL)lastBuffer;

typedef enum {Best, Good, Okay} Quality;

@property Quality quality;
@property int bitrate;

@end
