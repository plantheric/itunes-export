//
//  main.m
//  iTunesExport
//
//  Created by Mark Bartlett on 05/05/2013.
//  Copyright (c) 2013 plantheric. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
	return NSApplicationMain(argc, (const char **)argv);
}
