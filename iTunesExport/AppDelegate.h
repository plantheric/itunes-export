//
//  AppDelegate.h
//  iTunesExport
//
//  Created by Mark Bartlett on 05/05/2013.
//  Copyright (c) 2013 plantheric. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@end
