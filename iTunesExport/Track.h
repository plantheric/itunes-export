//
//  Track.h
//  iTunesExport
//
//  Created by Mark Bartlett on 12/05/2013.
//  Copyright (c) 2013 plantheric. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Track : NSObject

@property NSData *sampleData;

@property NSString *title;
@property NSString *artist;
@property NSString *album;
@property NSString *year;
@property NSString *genre;

@property NSData *artWork;


@end
