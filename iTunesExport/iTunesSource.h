//
//  iTunesSource.h
//  iTunesExport
//
//  Created by Mark Bartlett on 07/05/2013.
//  Copyright (c) 2013 plantheric. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iTunesSource : NSObject

- (id)init;
- (BOOL)loadLibrary;

- (NSArray*)userPlaylistNames;
- (NSArray*)tracksForMusicPlaylist;
- (NSArray*)tracksForPlaylistNamed:(NSString*)playlist;

@property (readonly) NSURL *libraryLocation;

@end
