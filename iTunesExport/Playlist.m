//
//  Playlist.m
//  iTunesExport
//
//  Created by Mark Bartlett on 12/05/2013.
//  Copyright (c) 2013 plantheric. All rights reserved.
//

#import "Playlist.h"
#import "NSArray+Extensions.h"

@interface Playlist()

@property NSURL *exportFolder;
@property NSURL *sourceFolder;

@end

@implementation Playlist

- (id)initWithSourceFolder:(NSURL*)sourceFolder exportFolder:(NSURL*) exportFolder {

	self = [super init];
	if (self) {
		self.exportFolder = exportFolder;
		self.sourceFolder = sourceFolder;
	}
	return self;
}

- (void)writePlaylistNamed:(NSString*)playlistName withTracks:(NSArray*)tracks {
	
	unsigned long iTunesLibraryBasePathLength = [[self.sourceFolder path] length]+1;

	NSArray *playlistItems = [tracks map:^(NSURL* url) {
		NSString *item =  [[[url path] substringFromIndex:iTunesLibraryBasePathLength] stringByDeletingPathExtension];
		item = [item stringByAppendingPathExtension:@"mp3"];
		item = [item stringByAppendingString:@"\n"];
		return item;
	}];

	NSMutableData *playlistContents = [NSMutableData data];

	NSString *header = [NSString stringWithFormat:@"#Playlist: '%@'\n", playlistName];
	[playlistContents appendData:[header dataUsingEncoding:NSUTF8StringEncoding]];
	
	for (NSString* item in playlistItems) {
		[playlistContents appendData:[item dataUsingEncoding:NSUTF8StringEncoding]];
	}

	NSURL *url = [[self.exportFolder URLByAppendingPathComponent:playlistName] URLByAppendingPathExtension:@"m3u"];
	[[NSFileManager defaultManager] createFileAtPath:[url path] contents:playlistContents attributes:nil];
}


@end
