//
//  Playlist.h
//  iTunesExport
//
//  Created by Mark Bartlett on 12/05/2013.
//  Copyright (c) 2013 plantheric. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Playlist : NSObject

- (id)initWithSourceFolder:(NSURL*)sourceFolder exportFolder:(NSURL*)exportFolder;
- (void)writePlaylistNamed:(NSString*)name withTracks:(NSArray*)tracks;

@end
