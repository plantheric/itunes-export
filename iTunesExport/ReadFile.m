//
//  ReadFile.m
//  iTunesExport
//
//  Created by Mark Bartlett on 10/05/2013.
//  Copyright (c) 2013 plantheric. All rights reserved.
//

#import "ReadFile.h"
#import "Track.h"
#include <CoreAudio/CoreAudioTypes.h>
#include <AudioToolbox/AudioQueue.h>
#include <AudioToolbox/ExtendedAudioFile.h>

@interface ReadFile() {
	
@private
	AudioFileID audioFileId;
	AudioStreamBasicDescription readFormat;
	AudioQueueRef audioQueue;
	AudioStreamPacketDescription *packetDescriptions;
    AudioChannelLayout *audioChannelLayout;
	long long packetCount;
	long long currentPacket;
	long long bufferByteSize;
	bool flushed;
	bool done;
	bool failed;
}

@property NSURL *sourceUrl;

@end

@implementation ReadFile

- (id)initWithURL:(NSURL *)url {
	self = [super init];
	if (self) {
		self.sourceUrl = url;
	}
	return self;
}

void readCallback (void *inUserData, AudioQueueRef inQueue, AudioQueueBufferRef inBuffer) {

	ReadFile* Self = (__bridge ReadFile *)(inUserData);

	[Self readForQueue:inQueue toBuffer:inBuffer];
}

- (void)readForQueue:(AudioQueueRef) inQueue toBuffer:(AudioQueueBufferRef) inBuffer {
	
	if (done)
		return;
	
	uint32_t numBytes = (uint32_t)bufferByteSize;
	uint32_t nPackets = (uint32_t)packetCount;
	
	OSStatus result = AudioFileReadPacketData(audioFileId, false, &numBytes,
											  packetDescriptions, currentPacket, &nPackets,
											  inBuffer->mAudioData);
	if (result) {
		failed = true;
		return;
	}
	
	if (nPackets > 0) {
		inBuffer->mAudioDataByteSize = numBytes;
		
		result = AudioQueueEnqueueBuffer(inQueue, inBuffer, (packetDescriptions ? nPackets : 0), packetDescriptions);
		if (result) {
			failed = true;
			return;
		}
		
		currentPacket += nPackets;
	} else {
		if (!flushed){
			result = AudioQueueFlush(inQueue);
			if (result) {
				failed = true;
				return;
			}
			flushed = true;
		}

		result = AudioQueueStop(inQueue, false);
		if (result) {
			failed = true;
			return;
		}
		done = true;
	}
}



- (Track*)track {

	OSStatus status = AudioFileOpenURL((__bridge CFURLRef)self.sourceUrl, kAudioFileReadPermission, 0, &audioFileId);
	if (status != noErr)
		return nil;
	
	Track *track = [[Track alloc] init];
			
	NSDictionary* tags = GetID3Tags (audioFileId);
	track.title = tags[@kAFInfoDictionary_Title];
	track.artist = tags[@kAFInfoDictionary_Artist];
	track.album = tags[@kAFInfoDictionary_Album];
	track.year = tags[@kAFInfoDictionary_Year];
	track.genre = tags[@kAFInfoDictionary_Genre];
	track.artWork = GetArtWork(audioFileId);
	
	return track;
}

- (id)read:(void(^)(NSData*, BOOL))write {

	UInt32 size = sizeof(readFormat);
	OSStatus status = AudioFileGetProperty(audioFileId, kAudioFilePropertyDataFormat, &size, &readFormat);
	if (status != noErr)
		return nil;
		
	status = AudioQueueNewOutput(&readFormat, readCallback, (__bridge void *)(self), CFRunLoopGetCurrent(), kCFRunLoopCommonModes, 0, &audioQueue);
	if (status != noErr)
		return nil;
	
	uint32_t maxPacketSize;
	size = sizeof(maxPacketSize);
	status = AudioFileGetProperty(audioFileId, kAudioFilePropertyPacketSizeUpperBound, &size, &maxPacketSize);
	if (status != noErr)
		return nil;
	
	size = sizeof(packetCount);
	status = AudioFileGetProperty(audioFileId, kAudioFilePropertyAudioDataPacketCount, &size, &packetCount);
	if (status != noErr)
		return nil;
	
	bufferByteSize = maxPacketSize * packetCount / 4;
	
	if (readFormat.mBytesPerPacket == 0 || readFormat.mFramesPerPacket == 0)
		packetDescriptions = malloc(packetCount * sizeof(AudioStreamPacketDescription));
	
	size = sizeof(UInt32);
	status = AudioFileGetPropertyInfo (audioFileId, kAudioFilePropertyMagicCookieData, &size, NULL);
	if (status != noErr)
		return nil;
	
	if (status == noErr && size > 0)
	{
		NSMutableData* cookie = [NSMutableData dataWithCapacity:size];
		status = AudioFileGetProperty(audioFileId, kAudioFilePropertyMagicCookieData, &size, cookie.mutableBytes);
		if (status != noErr)
			return nil;
		status = AudioQueueSetProperty(audioQueue, kAudioQueueProperty_MagicCookie, cookie.mutableBytes, size);
		if (status != noErr)
			return nil;
	}
	
	//	Channel layout
	status = AudioFileGetPropertyInfo(audioFileId, kAudioFilePropertyChannelLayout, &size, NULL);
	if (status == noErr && size > 0)
	{
		audioChannelLayout = (AudioChannelLayout *)malloc(size);
		AudioFileGetProperty(audioFileId, kAudioFilePropertyChannelLayout, &size, audioChannelLayout);
		AudioQueueSetProperty(audioQueue, kAudioQueueProperty_ChannelLayout, audioChannelLayout, size);
	}
	
	//	Allocate the Queue read buffer
	AudioQueueBufferRef audioQueueBuffer;
	status = AudioQueueAllocateBuffer(audioQueue, (uint32_t)bufferByteSize, &audioQueueBuffer);
	if (status != noErr)
		return nil;
	
	// prepare a canonical interleaved capture format
	
	AudioStreamBasicDescription captureFormat;
	captureFormat.mSampleRate = readFormat.mSampleRate;
	captureFormat.mFormatID = kAudioFormatLinearPCM;
	
	captureFormat.mFormatFlags = kAudioFormatFlagIsFloat | kAudioFormatFlagsNativeEndian | kAudioFormatFlagIsPacked;
	
	captureFormat.mBitsPerChannel = 8 * sizeof(Float32);
	captureFormat.mChannelsPerFrame = readFormat.mChannelsPerFrame;
	captureFormat.mFramesPerPacket = 1;
	captureFormat.mBytesPerPacket = captureFormat.mBytesPerFrame = readFormat.mChannelsPerFrame * sizeof(Float32);
	
	status = AudioQueueSetOfflineRenderFormat(audioQueue, &captureFormat, audioChannelLayout);
	if (status != noErr)
		return nil;
	
	//	Allocate the capture buffer
	const uint32_t captureBufferByteSize = (uint32_t)bufferByteSize;
	
	AudioQueueBufferRef captureBuffer;
	status = AudioQueueAllocateBuffer(audioQueue, captureBufferByteSize, &captureBuffer);
	if (status != noErr)
		return nil;
		
	// set the volume of the queue
	float volume = 1.;
	status = AudioQueueSetParameter(audioQueue, kAudioQueueParam_Volume, volume);
	if (status != noErr)
		return nil;
	
	status = AudioQueueStart(audioQueue, NULL);
	if (status != noErr)
		return nil;
	
	AudioTimeStamp timeStamp;
	timeStamp.mFlags = kAudioTimeStampSampleTimeValid;
	timeStamp.mSampleTime = 0;
	
	currentPacket = 0;
	
	// we need to call this once asking for 0 frames
	status = AudioQueueOfflineRender(audioQueue, &timeStamp, captureBuffer, 0);
	if (status != noErr)
		return nil;
	
	// we need to enqueue a buffer after the queue has started
	readCallback ((__bridge void *)(self), audioQueue, audioQueueBuffer);
	
	while (true) {
		
		uint32_t requiredFrames = captureBuffer->mAudioDataBytesCapacity / captureFormat.mBytesPerFrame;
		
		status = AudioQueueOfflineRender(audioQueue, &timeStamp, captureBuffer, requiredFrames);
		if (failed || status != noErr)
			break;
		
		uint32_t writeFrames = captureBuffer->mAudioDataByteSize / captureFormat.mBytesPerFrame;
		if (writeFrames == 0)
			break;

		write([NSData dataWithBytes:captureBuffer->mAudioData length:captureBuffer->mAudioDataByteSize], FALSE);
		if (flushed)
			break;

		timeStamp.mSampleTime += writeFrames;
	}

	write([NSData data], TRUE);
		
	CFRunLoopRunInMode(kCFRunLoopDefaultMode, 1, false);
	
	return nil;
}

- (void)dealloc {
	
	AudioQueueDispose(audioQueue, true);
	AudioFileClose(audioFileId);
	free(packetDescriptions);
	free(audioChannelLayout);
}

NSDictionary* GetID3Tags (AudioFileID audioFile) {

	CFDictionaryRef tagCFDict = nil;
	u_int32_t dataSize = sizeof(tagCFDict);

	AudioFileGetProperty(audioFile, kAudioFilePropertyInfoDictionary, &dataSize, &tagCFDict);
	NSDictionary *tagDict = CFBridgingRelease(tagCFDict);
	
	return tagDict;
}

NSData* GetArtWork (AudioFileID audioFile) {
	CFDataRef artWorkCFRef = nil;
	u_int32_t dataSize = sizeof(artWorkCFRef);
	
	AudioFileGetProperty(audioFile, kAudioFilePropertyAlbumArtwork, &dataSize, &artWorkCFRef);
	NSData *artWork = CFBridgingRelease(artWorkCFRef);
	
	return artWork;
}

@end
