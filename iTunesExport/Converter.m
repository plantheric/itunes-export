//
//  Converter.m
//  iTunesExport
//
//  Created by Mark Bartlett on 10/05/2013.
//  Copyright (c) 2013 plantheric. All rights reserved.
//

#import "Converter.h"
#import "ReadFile.h"
#import "WriteFile.h"
#import "Track.h"

@interface Converter()

@property NSURL *exportFolder;
@property NSURL *sourceFolder;

@end

@implementation Converter

- (id)initWithSourceFolder:(NSURL*)sourceFolder exportFolder:(NSURL*) exportFolder {
	self = [super init];
	if(self) {
		self.exportFolder = exportFolder;
		self.sourceFolder = sourceFolder;
		_exportStopping = NO;
	}
	return self;
}

- (void) exportFiles:(NSArray*)filesToExport withProgress:(void (^)(double, NSString*))progress {
	
	dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
	dispatch_group_t group = dispatch_group_create();
	dispatch_semaphore_t semaphore = dispatch_semaphore_create(4);
	
	for (int i=0; i< [filesToExport count]; i++) {
		
		if (self.exportStopping)
			break;

		dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
		
		NSURL *file = filesToExport[i];

		progress ((double)i/[filesToExport count], [file lastPathComponent]);

		dispatch_group_async(group, queue, ^{

			@autoreleasepool {
			
				NSURL *exportUrl = [self makeExportUrlFromSource:file];
				ReadFile *readFile = [[ReadFile alloc] initWithURL:file];
				Track *track = [readFile track];

				WriteFile *writeFile = [[WriteFile alloc] initWithURL:exportUrl track:track];
				writeFile.quality = self.mp3Quality;
				writeFile.bitrate = self.mp3Bitrate;

				if ([writeFile create]) {

					[readFile read:^(NSData* samples, BOOL lastBuffer) {
						[writeFile write:samples last:lastBuffer];
					}];
				}

				dispatch_semaphore_signal(semaphore);
			}
		});
	}
	dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
}

- (NSURL*) makeExportUrlFromSource:(NSURL*) sourceUrl {
	
	unsigned long sourceBasePathLength = [[self.sourceFolder path] length]+1;
	
	NSString *relative = [[[sourceUrl path] substringFromIndex:sourceBasePathLength] stringByDeletingPathExtension];
	
	NSURL *exportUrl = [self.exportFolder URLByAppendingPathComponent:relative];
	exportUrl = [exportUrl URLByAppendingPathExtension:@"mp3"];
	
	[[NSFileManager defaultManager] createDirectoryAtURL: [exportUrl URLByDeletingLastPathComponent]
							 withIntermediateDirectories: YES
											  attributes: nil
												   error: nil];
	return exportUrl;
}

- (void)stopExport {

	_exportStopping = YES;
}

@end
