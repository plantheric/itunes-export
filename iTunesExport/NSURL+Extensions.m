//
//  NSURL+Extensions.m
//  iTunesExport
//
//  Created by Mark Bartlett on 14/05/2013.
//  Copyright (c) 2013 plantheric. All rights reserved.
//

#import "NSURL+Extensions.h"

@implementation NSURL (Extensions)

- (NSString*)safePath {
	NSString *path = [self path];
	return path ? path : @"";
}


@end
