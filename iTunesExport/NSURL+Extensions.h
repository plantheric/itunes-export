//
//  NSURL+Extensions.h
//  iTunesExport
//
//  Created by Mark Bartlett on 14/05/2013.
//  Copyright (c) 2013 plantheric. All rights reserved.
//

@interface NSURL (Extensions)

- (NSString*)safePath;

@end
