//
//  NSArray+Extensions.h
//  iTunesExport
//
//  Created by Mark Bartlett on 15/12/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

@interface NSArray (Extensions)

- (NSArray*)remove:(BOOL(^)(id obj))block;
- (NSArray*)map:(id(^)(id obj))block;
- (BOOL)any:(BOOL(^)(id obj))block;
- (void)each:(void(^)(id obj))block;
- (id)first:(BOOL(^)(id obj))block;
- (NSArray*)filter:(BOOL(^)(id obj))block;

@end
