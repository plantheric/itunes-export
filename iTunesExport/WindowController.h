//
//  WindowController.h
//  iTunesExport
//
//  Created by Mark Bartlett on 07/05/2013.
//  Copyright (c) 2013 plantheric. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface WindowController : NSWindowController<NSWindowDelegate>

@property (weak) IBOutlet NSTextField *libraryLocation;
@property (weak) IBOutlet NSTextField *exportLocation;
- (IBAction)changeExportFolder:(id)sender;
@property (weak) IBOutlet NSTextField *filesToExportCount;

- (IBAction)exportFiles:(id)sender;
@property (weak) IBOutlet NSButton *exportButton;

- (IBAction)cancelExport:(id)sender;
@property (weak) IBOutlet NSButton *cancelButton;

@property (weak) IBOutlet NSProgressIndicator *progress;

@property (weak) IBOutlet NSTextField *exportStatus;

@property (weak) IBOutlet NSPopUpButton *bitrate;
@property (weak) IBOutlet NSPopUpButton *quality;
@end
