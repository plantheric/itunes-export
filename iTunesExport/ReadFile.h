//
//  ReadFile.h
//  iTunesExport
//
//  Created by Mark Bartlett on 10/05/2013.
//  Copyright (c) 2013 plantheric. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Track;

@interface ReadFile : NSObject

- (id)initWithURL:(NSURL*)url;
- (Track*)track;
- (id)read:(void(^)(NSData*, BOOL))write;

@end
