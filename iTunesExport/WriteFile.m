//
//  WriteFile.m
//  iTunesExport
//
//  Created by Mark Bartlett on 11/05/2013.
//  Copyright (c) 2013 plantheric. All rights reserved.
//

#import "WriteFile.h"
#import "Track.h"
#include "LAME/lame.h"

@interface WriteFile()

@property NSURL *exportURL;
@property lame_global_flags *globalFlags;
@property NSMutableData* mp3buffer;
@property FILE *exportFile;
@property Track *track;

@end

@implementation WriteFile

- (id)initWithURL:(NSURL*)url track:(Track*)track {
	self = [super init];
	if (self) {
		self.exportURL = url;
		self.quality = Best;
		self.bitrate = 128;
		self.track = track;
		self.mp3buffer = [NSMutableData data];
	}
	return self;
}

static int qualities[] = {2, 5, 7};

- (BOOL)create {
	
	if (self.track == nil)
		return FALSE;

	self.globalFlags = lame_init();

	id3tag_set_title(self.globalFlags, [self.track.title UTF8String]);
	id3tag_set_artist(self.globalFlags, [self.track.artist UTF8String]);
	id3tag_set_album(self.globalFlags, [self.track.album UTF8String]);
	id3tag_set_year(self.globalFlags, [self.track.year UTF8String]);
	id3tag_set_genre(self.globalFlags, [self.track.genre UTF8String]);
	//	id3tag_set_albumart(self.globalFlags, [track.artWork bytes], [track.artWork length]);
	
	lame_set_brate(self.globalFlags, self.bitrate);
	lame_set_quality(self.globalFlags, qualities[self.quality]);
	
	if (lame_init_params(self.globalFlags) == -1)
		return FALSE;
	
	self.exportFile = fopen([[self.exportURL path] UTF8String] , "wb+");
	if (self.exportFile == NULL)
		return FALSE;
	
	return TRUE;
}

- (BOOL)write:(NSData*)sampleData last:(BOOL)lastBuffer{
	
	if (sampleData == nil) {
		return FALSE;
	}

	long sampleCount = sampleData.length/(2 * sizeof(float));
	if (sampleCount > INT_MAX)
		return FALSE;
	
	int mp3buffersize =  (1.25 * sampleCount) + 7200;
	if (mp3buffersize > self.mp3buffer.length)
		[self.mp3buffer setLength:mp3buffersize];

	int bytes = lame_encode_buffer_interleaved_ieee_float(self.globalFlags, sampleData.bytes, (int)sampleCount, [self.mp3buffer mutableBytes], mp3buffersize);

	if (bytes > 0)
		fwrite([self.mp3buffer bytes], 1, bytes, self.exportFile);

	if (lastBuffer) {
		bytes = lame_encode_flush(self.globalFlags, [self.mp3buffer mutableBytes], mp3buffersize);

		if (bytes > 0)
			fwrite([self.mp3buffer bytes], 1, bytes, self.exportFile);

		lame_mp3_tags_fid(self.globalFlags, self.exportFile);
	}
		
	return TRUE;
}

- (void)dealloc {
	
	if (self.exportFile != NULL) {
		fflush(self.exportFile);
		fpurge(self.exportFile);
		fclose(self.exportFile);
	}

	lame_close(self.globalFlags);
}

@end
