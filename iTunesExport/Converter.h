//
//  Converter.h
//  iTunesExport
//
//  Created by Mark Bartlett on 10/05/2013.
//  Copyright (c) 2013 plantheric. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Converter : NSObject

- (id)initWithSourceFolder:(NSURL*)sourceFolder exportFolder:(NSURL*) exportFolder;
- (void)exportFiles:(NSArray*)filesToConvert withProgress:(void(^)(double, NSString*))progress;
- (void)stopExport;

@property (readonly) BOOL exportStopping;

@property int mp3Quality;
@property int mp3Bitrate;

@end
