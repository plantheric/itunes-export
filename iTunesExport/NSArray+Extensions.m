//
//  NSArray+Extensions.m
//  iTunesExport
//
//  Created by Mark Bartlett on 15/12/2012.
//  Copyright (c) 2012 plantheric. All rights reserved.
//

#import "NSArray+Extensions.h"

@implementation NSArray (Extensions)

- (NSArray*)remove:(BOOL(^)(id))block {
	NSMutableArray *newArray = [NSMutableArray arrayWithCapacity:[self count]];
	
	[self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		if (block(obj) == NO)
			[newArray addObject:obj];
	}];
	
	return newArray;
}

- (NSArray*)map:(id(^)(id obj))block {
	NSMutableArray *newArray = [NSMutableArray arrayWithCapacity:[self count]];
	
	[self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		[newArray addObject:block(obj)];
	}];
	
	return newArray;
	
}

- (BOOL)any:(BOOL(^)(id obj))block {
	__block BOOL any = FALSE;

	[self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		if (block(obj) == YES) {
			any = TRUE;
			*stop = TRUE;
		}
	}];
	
	return any;
}

- (void)each:(void(^)(id obj))block {
	
	[self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		block(obj);
	}];
}

- (id)first:(BOOL(^)(id obj))block {
	__block id first = nil;
	
	[self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		if (block(obj) == YES) {
			first = obj;
			*stop = TRUE;
		}
	}];
	
	return first;
}

- (NSArray*)filter:(BOOL(^)(id))block {
	NSMutableArray *newArray = [NSMutableArray arrayWithCapacity:[self count]];
	
	[self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		if (block(obj) == YES)
			[newArray addObject:obj];
	}];
	
	return newArray;
}

@end
